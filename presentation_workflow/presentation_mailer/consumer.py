import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
from django.core.mail import send_mail
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    data = json.loads(body)
    email = data["presenter_email"]
    name = data["presenter_name"]
    title = data["title"]

    send_mail(
        "Your presentation has been approved",
        f'{name}, we are sorry to tell you that your presentation {title} was approved',
        "admin@conference.go",
        [email],
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    data = json.loads(body)
    email = data["presenter_email"]
    name = data["presenter_name"]
    title = data["title"]

    send_mail(
        "Your presentation has been rejected",
        f'{name}, we are sorry to tell you that your presentation {title} was rejected',
        "admin@conference.go",
        [email],
        fail_silently=False,
    )
    print("reject")


while True:
    try:
        parameters = pika.ConnectionParameters(host='rabbitmq')
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue='presentation_approvals')
        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.queue_declare(queue='presentation_rejections')
        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        print("consuming")
        channel.start_consuming()
    except AMQPConnectionError:
        print("failure to connect")
        time.sleep(2.00)

