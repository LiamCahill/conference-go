from django.http import JsonResponse
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json
from .models import Location


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": f"{city} {state}",
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except:
        return {"picture_url": None}


def get_weather_data(city, state):
    headers = {"Authorization": OPEN_WEATHER_API_KEY}
    params = {
        # "q": city + " " state, US,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct?q={city name},{state code},{country code}&limit={limit}&appid={API key}"
